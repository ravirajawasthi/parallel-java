package week1;

import utils.TimeIt;

import static java.util.FormatProcessor.FMT;

public class SequentialSum {
    public static int findSumSequentially(int[] arr, TimeIt timeit) {
        timeit.startTime();
        int sum = 0;
        for (int v : arr) sum += v;
        System.out.println(timeit.endTime(FMT."calculated sum = %d\{sum} sequentially"));
        return sum;
    }


    public static float findSumSequentially(float[] arr, TimeIt timeit) {
        timeit.startTime();
        float sum = 0;
        for (float v : arr) sum += v;
        System.out.println(timeit.endTime(FMT."calculated sum = %d\{sum} sequentially"));
        return sum;
    }
}
