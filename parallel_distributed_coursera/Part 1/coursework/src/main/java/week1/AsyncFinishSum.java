package week1;

import utils.TimeIt;

import java.security.SecureRandom;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicReference;

import static edu.rice.pcdp.PCDP.async;
import static edu.rice.pcdp.PCDP.finish;
import static java.util.FormatProcessor.FMT;
import static week1.SequentialSum.findSumSequentially;

public class AsyncFinishSum {
    static TimeIt timeit = new TimeIt();

    static SecureRandom random = new SecureRandom();


    public static void main(String[] args) {

        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", String.valueOf(16));

        timeit.startTime();
        int[] arr = prepArray(100_000_000);
        System.out.println(timeit.endTime("initializing sum array"));

        for (int i = 1; i < 6; i++) {
            System.out.println(STR."Iteration \{i}");
            findSumSequentially(arr, timeit);
            findSumParallel(arr);

            timeit.startTime();
            var forkJoin = new ForkJoinAsyncSum(0, arr.length, arr);
            ForkJoinPool.commonPool().invoke(forkJoin);
            System.out.println(timeit.endTime(FMT."""
                    Calculated sum = \{forkJoin.ans} : parallely"""));

            System.out.println();
        }


    }

    private static int[] prepArray(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(30);
        }
        return arr;
    }


    private static void findSumParallel(int[] arr) {
        timeit.startTime();
        AtomicReference<Long> sum1 = new AtomicReference<>((long) 0);
        AtomicReference<Long> sum2 = new AtomicReference<>((long) 0);
        finish(() -> {
            async(() -> {
                long tempSum1 = 0;
                for (int i = 0; i < arr.length / 2; i++) {
                    tempSum1 += arr[i];
                }
                sum1.set(tempSum1);
            });

            async(() -> {
                long tempSum2 = 0;
                for (int i = arr.length / 2; i < arr.length; i++) {
                    tempSum2 += arr[i];
                }
                sum2.set(tempSum2);
            });


        });
        long sum = sum1.get() + sum2.get();
        System.out.println(timeit.endTime(FMT."calculated sum = %d\{sum} parallel"));
    }


}
