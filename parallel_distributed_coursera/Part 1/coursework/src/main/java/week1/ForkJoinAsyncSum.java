package week1;

import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;


class ForkJoinAsyncSum extends RecursiveAction {

    int startIndex;
    int endIndex;

    int[] arr;
    int ans;

    ForkJoinAsyncSum(int startIndex, int endIndex, int[] arr) {
        super();
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.arr = arr;
        ans = 0;
    }

    /**
     * The main computation performed by this task.
     */
    @Override
    protected void compute() {
        int SEQ_THRESHOLD = 1000;
        if (endIndex - startIndex < SEQ_THRESHOLD) {
            for (int i = startIndex; i < endIndex; i++) this.ans += arr[i];
        } else {
            var left = new ForkJoinAsyncSum(startIndex, (startIndex + endIndex) / 2, arr);
            var right = new ForkJoinAsyncSum((endIndex + startIndex) / 2, endIndex, arr);
            left.fork();
            right.compute();
            left.join();
            this.ans = left.ans + right.ans;
        }

    }
}




