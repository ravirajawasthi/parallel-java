package utils;

import java.sql.Time;

import static java.util.FormatProcessor.FMT;

public class TimeIt {

    private long startTime;

    public TimeIt() {
    }

    public void startTime() {
        startTime = System.nanoTime();
    }

    public String endTime(String message) {
        return FMT."""
                Time taken for :\{message} : %8.10f\{(System.nanoTime() - startTime) / 1000000000.0} seconds""";
    }

    public String endTime() {
        return FMT."%8.10f\{(System.nanoTime() - startTime) / 1000000000.0} seconds";
    }

}
